package com.example.truongpq.assistivetouch;

import android.app.Dialog;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class HomeDialog extends Dialog {
    private static final String normal = "Normal";
    private static final String vibarate = "Vibrate";
    private static final String silent = "Silent";

    private Context context;

    public HomeDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_menu);

        //getWindow().setBackgroundDrawableResource();

        home();
        ringerMode();
        settings();
        lock();
    }

    private void ringerMode() {
        final TextView tvMode = (TextView) findViewById(R.id.tv_mode);
        final ImageView btnRingerMode = (ImageView) findViewById(R.id.btn_ringer_mode);

        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        switch (audioManager.getRingerMode()) {
            case 0:
                btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_silent);
                tvMode.setText(silent);
                break;
            case 1:
                btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_vibrate);
                tvMode.setText(vibarate);
                break;
            case 2:
                btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_normal);
                tvMode.setText(normal);
                break;
        }

        btnRingerMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mode = (audioManager.getRingerMode() + 1) % 3;
                switch (mode) {
                    case 0:
                        audioManager.setRingerMode(0);
                        btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_silent);
                        tvMode.setText(silent);
                        break;
                    case 1:
                        audioManager.setRingerMode(1);
                        btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_vibrate);
                        tvMode.setText(vibarate);
                        break;
                    case 2:
                        audioManager.setRingerMode(2);
                        btnRingerMode.setImageResource(R.drawable.ic_ringer_mode_normal);
                        tvMode.setText(normal);
                        break;
                }
            }
        });
    }

    private void home() {
        ImageView btnHome = (ImageView) findViewById(R.id.btn_home);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(startMain);
                dismiss();

//                Intent intent = new Intent("com.android.systemui.recent.action.TOGGLE_RECENTS");
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                ComponentName recents = new ComponentName("com.android.systemui", "com.android.systemui.recent.RecentsActivity");
//                intent.setComponent(recents);
//                context.startActivity(intent);
//                dismiss();
            }
        });
    }

    private void settings() {
        ImageView btnSetting = (ImageView) findViewById(R.id.ic_setting);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new SettingDialog(context)).show();
                dismiss();
            }
        });
    }

    private void lock() {
        ImageView btnLock = (ImageView) findViewById(R.id.btn_lock);
        btnLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DevicePolicyManager policyManager = (DevicePolicyManager)  context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                ComponentName componentName = new ComponentName(context, DeviceAdminReceiver.class.getName());
                if (policyManager.isAdminActive(componentName)) {
                    policyManager.lockNow();
                }
                dismiss();
            }
        });
    }

}
