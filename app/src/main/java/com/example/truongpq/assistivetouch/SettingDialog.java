package com.example.truongpq.assistivetouch;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingDialog extends Dialog {
    private Context context;

    public SettingDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_setting);

        back();
        autoRotate();
        volumeControl();
        location();
        wifi();
        airPlaneMode();
        bluetooth();
    }

    public boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    private void autoRotate() {
        final ImageView btnRotation = (ImageView) findViewById(R.id.btn_rotation);
        final TextView tvRotation = (TextView) findViewById(R.id.tv_rotation);
        if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
            btnRotation.setImageResource(R.drawable.ic_screen_rotation);
            tvRotation.setText("Auto-rotate");
        } else {
            btnRotation.setImageResource(R.drawable.ic_screen_lock_rotation);
            tvRotation.setText("Potrait");
        }
        btnRotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
                    android.provider.Settings.System.putInt(context.getContentResolver(),Settings.System.ACCELEROMETER_ROTATION, 0);
                    btnRotation.setImageResource(R.drawable.ic_screen_lock_rotation);
                    tvRotation.setText("Potrait");
                } else {
                    android.provider.Settings.System.putInt(context.getContentResolver(),Settings.System.ACCELEROMETER_ROTATION, 1);
                    btnRotation.setImageResource(R.drawable.ic_screen_rotation);
                    tvRotation.setText("Auto-rotate");
                }
            }
        });
    }

    private void back() {
        ImageView btnBack = (ImageView) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new HomeDialog(context)).show();
                dismiss();
            }
        });
    }

    private void airPlaneMode() {
        ImageView btnAirplanemode = (ImageView) findViewById(R.id.btn_airplanemode);
        if (isAirplaneModeOn(context)) {
            btnAirplanemode.setImageResource(R.drawable.ic_airplanemode_enable);
        } else {
            btnAirplanemode.setImageResource(R.drawable.ic_airplanemode_disable);
        }
        btnAirplanemode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                dismiss();
            }
        });
    }

    private void volumeControl() {
        ImageView btnVolumeUp = (ImageView) findViewById(R.id.btn_volume_up);
        btnVolumeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
            }
        });

        ImageView btnVolumeDown = (ImageView) findViewById(R.id.btn_volume_down);
        btnVolumeDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
            }
        });
    }

    private void location() {
        final ImageView btnLocation = (ImageView) findViewById(R.id.btn_location);
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            btnLocation.setImageResource(R.drawable.ic_location_enable);
        } else {
            btnLocation.setImageResource(R.drawable.ic_location_disable);
        }
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                dismiss();
            }
        });
    }

    private void wifi() {
        final ImageView btnWifi = (ImageView) findViewById(R.id.btn_wifi);
        if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.Global.WIFI_ON, 0) == 1) {
            btnWifi.setImageResource(R.drawable.ic_wifi_enable);
        } else {
            btnWifi.setImageResource(R.drawable.ic_wifi_disable);
        }
        btnWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.Global.WIFI_ON, 0) == 1) {
                    wifiManager.setWifiEnabled(false);
                    btnWifi.setImageResource(R.drawable.ic_wifi_disable);
                } else {
                    wifiManager.setWifiEnabled(true);
                    btnWifi.setImageResource(R.drawable.ic_wifi_enable);
                }
            }
        });
    }

    private void bluetooth() {
        final ImageView btnBluetooth = (ImageView) findViewById(R.id.btn_bluetooth);
        if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.Global.BLUETOOTH_ON, 0) == 1) {
            btnBluetooth.setImageResource(R.drawable.ic_bluetooth_enable);
        } else {
            btnBluetooth.setImageResource(R.drawable.ic_bluetooth_disable);
        }
        btnBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (bluetoothAdapter != null) {
                    if (android.provider.Settings.System.getInt(context.getContentResolver(),Settings.Global.BLUETOOTH_ON, 0) == 1) {
                        bluetoothAdapter.disable();
                        btnBluetooth.setImageResource(R.drawable.ic_bluetooth_disable);
                    } else {
                        bluetoothAdapter.enable();
                        btnBluetooth.setImageResource(R.drawable.ic_bluetooth_enable);
                    }
                }
            }
        });
    }

    private void brightness() {

    }

    private void flashlight() {

    }
}
