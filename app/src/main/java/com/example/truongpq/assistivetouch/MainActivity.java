package com.example.truongpq.assistivetouch;

import android.app.ActivityManager;
import android.app.Dialog;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String stateOn = "ENABLE";
    private static final String stateOff = "DISABLE";

    private TextView tvOnOff;
    private Intent buttonService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activeManage();

        buttonService = new Intent(this, HomeButtonService.class);
        intView();
    }

    private void intView() {
        tvOnOff = (TextView) findViewById(R.id.tv_on_off);
        displayState();

        ImageView btnOnOff = (ImageView) findViewById(R.id.btn_on_off);
        btnOnOff.setOnClickListener(this);

        ImageView btnMenuColor = (ImageView) findViewById(R.id.btn_menu_color);
        btnMenuColor.setOnClickListener(this);

        ImageView btnSetting = (ImageView) findViewById(R.id.btn_settings);
        btnSetting.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_on_off:
                if (isServiceRunning()) {
                    stopService(buttonService);
                    displayState();
                } else {
                    startService(buttonService);
                    displayState();
                }
                break;
            case R.id.btn_menu_color:
                Dialog menuColorDialog = new BackgroundMenuDialog(this);
                menuColorDialog.show();
                break;
            case R.id.btn_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void displayState() {
        if (isServiceRunning()) {
            tvOnOff.setText(stateOn);
        } else {
            tvOnOff.setText(stateOff);
        }
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (HomeButtonService.class.getName().equals(service.service.getClassName())) return true;
        }
        return false;
    }

    private void activeManage() {
        DevicePolicyManager policyManager = (DevicePolicyManager)  getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName componentName = new ComponentName(this, DeviceAdminReceiver.class.getName());
        if (!policyManager.isAdminActive(componentName)) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "");
            startActivityForResult(intent, 1);
        }
    }
}
